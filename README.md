# bash-programming

I attended 2024 Texas Linuxfest and had the opportunity to catch a good session
about bash programming presented by Cody Lee Cochran. He introduced me to linting 
and pep related tools for bash.

It inspired me to put some of the tools to use in this repository.

The original presentation can be found at:  https://bash.codrcodz.io/

Thanks Cody!

---

## shdoc

An autodoc generator for bash. At this time shdoc is available on Arch or as a github
repository:  https://github.com/reconquest/shdoc

### shdoc usage

```
shdoc < lib.sh > lib_doc.md
```

## bashate

A pep8 equivalent for bash scripts. The program attempts to be an automated style
checker for bash scripts. Written in python.

### bashate usage

```
bashate script.sh
```

## shellcheck 

Finds bugs in your shell scripts. Available in most package managers.

### shellcheck usage

```
shellcheck myscript.sh
```

---  

That is all.

--- 

