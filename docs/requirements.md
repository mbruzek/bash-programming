# requirements

A script to install the requirements for this repository.

## Overview

Run this script to install the requirements for this repository.
Some of the requirements are installed using a package manager and some are
cloned from git repositories and installed.

## Index

* [apt_install_packages](#aptinstallpackages)
* [git_clone_make_install](#gitclonemakeinstall)

### apt_install_packages

Apt install packages
This function installs apt packages using sudo.

#### Arguments

* **...** (string): The debian packages to install

### git_clone_make_install

Clone and make install the string of git urls.
This function git clones and make installs a space separated string of URLs.

#### Arguments

* **...** (string): The urls to `git clone` and sudo make install.

