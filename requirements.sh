#!/usr/bin/env bash
# @file requirements
# @brief A script to install the requirements for this repository.
# @description Run this script to install the requirements for this repository.
# Some of the requirements are installed using a package manager and some are
# cloned from git repositories and installed.

APT_PKGS="git python3-bashate shellcheck"
GIT_URLS="https://github.com/reconquest/shdoc"

# @description Apt install packages
# This function installs apt packages using sudo.
# @arg $@ string The debian packages to install
apt_install_packages() {
    echo "Apt installing packages $*"
    # shellcheck disable=SC2048,SC2086
    sudo apt install -y $*
}

# @description Clone and make install the string of git urls.
# This function git clones and make installs a space separated string of URLs.
# @arg $@ string The urls to `git clone` and sudo make install.
git_clone_make_install() {
    # shellcheck disable=SC2048
    for url in $*; do
        pushd "$PWD" || break;
        echo "Cloning git repository $url"
        git clone --recursive "$url"
        dir=$(basename "$url")
        cd "$dir" || break;
        echo "cd $dir; sudo make install"
        sudo make install
        popd || break;
    done
}

apt_install_packages "$APT_PKGS"
git_clone_make_install "$GIT_URLS"
